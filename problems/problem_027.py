# Complete the max_in_list function to find the
# maximum value in a list
#
# If the list is empty, then return None.
#

def max_in_list(values):
    if values:
        return max(values)
    else:
        return None
    # pass


test = [1, 2, 3]
print(max_in_list(test))
