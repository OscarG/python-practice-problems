# Write a function that meets these requirements.
#
# Name:       halve_the_list
# Parameters: a single list
# Returns:    two lists, each containing half of the original list
#             if the original list has an odd number of items, then
#             the extra item is in the first list
#
# Examples:
#    * input: [1, 2, 3, 4]
#      result: [1, 2], [3, 4]
#    * input: [1, 2, 3]
#      result: [1, 2], [3]

def halve_the_list(list):
    half_way_point = int(len(list) / 2)
    first_half = []
    second_half = []
    for item in range(0, len(list[0:half_way_point])):
        first_half.append(list[item])
    for item in range(len(list[half_way_point:len(list)]), len(list)):
        second_half.append(list[item])
    return first_half, second_half


test = [1, 2, 3, 4]
halve_the_list(test)
