# Complete the pairwise_add function which accepts two lists
# of the same size. It creates a new list and populates it
# with the sum of corresponding entries in the two lists.
#
# Examples:
#   * list1:  [1, 2, 3, 4]
#     list2:  [4, 5, 6, 7]
#     result: [5, 7, 9, 11]
#   * list1:  [100, 200, 300]
#     list2:  [ 10,   1, 180]
#     result: [110, 201, 480]
#
# Look up the zip function to help you with this problem.

def pairwise_add(list1, list2):
    new_list = []
    for item in range(0, len(list1)):
        number = list1[item] + list2[item]
        new_list.append(number)
    return new_list
    # pass


test1 = [1, 2, 3, 4]
test2 = [4, 5, 6, 7]
print(pairwise_add(test1, test2))
