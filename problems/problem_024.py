# Complete the calculate_average function which accepts
# a list of numerical values and returns the average of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#
# Pseudocode is available for you

def calculate_average(values):
    if values:
        return sum(values) / len(values)
    else:
        return None
    # pass


list = [10, 2, 3]
test = calculate_average(list)
print(test)
