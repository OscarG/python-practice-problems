# Complete the minimum_value function so that returns the
# minimum of two values.
#
# If the values are the same, return either.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def minimum_value(value1, value2):
    min = 0
    if value1 < value2:
        min += value1
    elif value2 < value1:
        min += value2
    else:
        min += value1
    print(min)
    pass


minimum_value(5, 2)
