# Complete the calculate_sum function which accepts
# a list of numerical values and returns the sum of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#

def calculate_sum(values):
    if values:
        return sum(values)
    else:
        return None
    # pass


test = [1, 6, 8]
print(calculate_sum(test))
